# -*- coding: utf-8 -*-

import torch
import numpy as np
from hddc import HDDC
from sklearn.metrics import accuracy_score as  OA

class HDDA(HDDC):
    """
    This class implements the HDDA  models proposed by Charles Bouveyron
    and Stephane Girard. It is based on my implementation of HDDC
    Details about methods can be found here:
    https://doi.org/10.1016/j.csda.2007.02.009
    """

    def __init__(self, C=None, th=0.1, model='M1', dtype=torch.float):
        super().__init__(C=C,
                         th=th,
                         model=model,
                         dtype=dtype,
                         init="user",
                         itermax=0)
    
    def score(self, X, y):
        return OA(y, self.predict(X))

# -*- coding: utf-8 -*-
# TODO: Check dtype for X in fit function

import torch
import numpy as np
from collections import defaultdict

# Constants
EPS = 1e-16


class HDDC():
    """
    This class implements the HDDC models proposed by Charles Bouveyron
    and Stephane Girard
    Details about methods can be found here:
    https://doi.org/10.1016/j.csda.2007.02.009
    """

    def __init__(self, model='M1', th=0.1, init='random',
                 itermax=100, tol=0.001, C=None,
                 check_empty=False, population=None,
                 dtype=torch.float, device=torch.device("cpu"),
                 subspace_estimation="cattel",
                 random_state=0):
        """
        This function initialize the HDDA stucture
        :param model: the model used.
        :type model: string
        - M1 = aijbiQidi
        - M2 = aijbiQid
        - M3 = aijbQidi
        - M4 = aijbQid
        - M5 = aibiQidi
        - M6 = aibiQid
        - M7 = aibQidi
        - M8 = aibQid
        - M9 = abiQidi <--
        - M10 = abiQid
        - M11 = abQidi
        - m12 = abQid
        """

        # Hyperparameters
        if model in ('M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8'):
            self.model = model  # Name of the model
        else:
            print("Model parameter {} is not available".format(model))
            exit()

        self.n_samples = None
        self.n_features = None
        if torch.is_tensor(th):
            self.th = th
        else:
            self.th = torch.tensor(th, dtype=dtype, device=device)
        self.init = init
        self.itermax = itermax
        self.tol = tol
        self.C = C
        self.population = population
        self.check_empty = check_empty
        self.subspace_estimation = subspace_estimation
        if C is None:
            self.C_ = []
        else:
            self.C_ = [C]
        self.device = device
        self.dtype = dtype

        self.q = []           # Number of parameters of the full models
        self.LL = []          # log likelyhood
        self.bic = []         # bic values of the model
        self.aic = []         # aic values of the model
        self.icl = []         # icl values of the model
        self.random_state = random_state

    def _init_list(self):
        self.ni = []
        self.prop = []
        self.mean = []
        self.pi = []
        self.trace = []
        self.logdet = []
        self.L = []
        self.Q = []
        self.a = []
        self.b = []

    def _init_clusters(self, y=None):
        # TODO: Add check for y \in [0, C-1]
        # TODO: Add checl for y type -> must be long
        if self.C == 1:
            self.T = torch.ones(self.n_samples, 1,
                                dtype=self.dtype, device=self.device)
        else:
            self.T = torch.zeros(self.n_samples, self.C,
                                 dtype=self.dtype, device=self.device)
            if self.init == 'random':
                torch.manual_seed(self.random_state)
                label = torch.randint(low=0, high=self.C,
                                      size=(self.n_samples,),
                                      dtype=torch.long, device=self.device)
            elif (self.init == "user") & (y is not None):
                label = y
            self.T[torch.arange(self.n_samples), label] = 1

    def _init_eigenvalues_common_noise_models(self, X):
        # Compute the gobal covariance matrix
        X_ = X - torch.mean(X, dim=0, keepdim=True)
        W = X_.t().mm(X)/self.n_samples
        L = torch.linalg.eigvalsh(W)
        # Check for numerical errors
        L.clamp(min=EPS)
        L = L.sort(descending=True)[0]
        self.dL = (L[1:]-L[:-1]).abs()
        self.dL /= self.dL.max()
        del L, W

    def fit(self, X, y=None):
        """Estimate the model parameters with the EM algorithm

        Parameters
        ----------
        X : tensor, shape (n_samples, n_features)
            List of n_features-dimensional data points. Each row
            corresponds to a single data point.
        Returns
        -------
        self
        """
        # TODO: Add check for ndim == 2 and X.dtype ==self.dtype

        # Initialization
        self.n_samples, self.n_features = X.shape
        self.niter = 1        # Number of iterations

        # Pre compute constant
        self.cst = self.n_features * np.log(2*np.pi)

        # Set minimum clusters size
        # Rule of dumbs for minimal size pi = 1 :
        # 1 mean vector (d) + 1 eigenvalue/vector (1+d) + 1 noise term ~ 2(d+1)
        if self.population is None:
            self.population = 2*(self.n_features + 1)

        if self.check_empty and (self.population > self.n_samples/self.C):
            print("Number of classes too high w.r.t the number of samples:"
                  "C should be deacreased")
            exit()

        # Init tensors and lists
        self._init_list()

        # Clusters initialization
        self._init_clusters(y=y)

        # Compute the whole covariance matrix and its eigenvalues if needed
        if self.model in ('M2', 'M4', 'M6', 'M8'):
            self._init_eigenvalues_common_noise_models(X)

        # First Steps
        self.m_step(X)
        self.LL.append(self.e_step(X))

        # Main while loop
        while(self.niter <= self.itermax):
            # M Step
            self.free()
            self.m_step(X)

            # E Step
            self.LL.append(self.e_step(X))

            # Update iterations
            self.niter += 1

            # Check condition
            if (np.absolute((self.LL[-1] - self.LL[-2]) /
                            self.LL[-2]) < self.tol) and \
                            (self.C_[-2] == self.C_[-1]):
                break

        # Compute some parameters of the optimization
        self.compute_nb_parameters()
        self.bic = - 2*self.LL[-1] + self.q*np.log(self.n_samples)
        self.aic = - 2*self.LL[-1] + 2*self.q
        self.icl = self.bic - 2*torch.log(self.T.max(1)[0]+EPS).sum()

        # Return clusters
        self.labels_ = self.T.argmax(1)
        return self

    def m_step(self, X):
        """"M step of the algorithm

        This function  computes the  empirical estimators of  the mean
        vector,  the convariance  matrix  and the  proportion of  each
        class. Then it estimates the signal and noise parameters.

        """
        # Learn model for each class
        C_ = self.C
        c_keep = []
        # Compute proportion
        ni = self.T.sum(0)

        for c in range(self.C):
            # Check if empty
            if self.check_empty and ni[c] < self.population:
                C_ -= 1
            else:
                self.ni.append(ni[c])
                self.prop.append(ni[c]/self.n_samples)
                self.mean.append(torch.matmul(self.T[:, c], X)/self.ni[-1])
                X_ = (X - self.mean[-1]) * \
                    torch.sqrt(self.T[:, c]).reshape(-1, 1)

                # Compute covariance matrix and eigendecomposition
                cov = X_.t().mm(X_)/(ni[c]-1)
                del X_
                L, Q = torch.linalg.eigh(cov)

                # Check for numerical errors
                L.clamp(min=EPS)
                if self.check_empty and torch.allclose(L.max(),
                                                       L.min()):
                    # In that case all eigenvalues are equal
                    # and this does not match the model, the cluster is removed
                    C_ -= 1
                    del self.ni[-1]
                    del self.prop[-1]
                    del self.mean[-1]
                else:
                    c_keep.append(c)
                    idx = L.argsort(descending=True)
                    L, Q = L[idx], Q[:, idx]

                    self.L.append(L)
                    self.Q.append(Q)
                    self.trace.append(L.sum())
        # Update T
        if C_ < self.C:
            self.T = torch.index_select(self.T, 1, torch.tensor(c_keep))

        # Update the number of clusters
        self.C_.append(C_)
        self.C = C_

        # Estimation of the signal subspace for specific size subspace models
        self.signal_subspace_estimation()

        # Estim signal part
        self.estim_signal_subspace()

        # Estim noise term
        self.estim_noise_subspace()

        # Compute remainings parameters
        # Precompute logdet
        self.logdet = [(torch.log(sA).sum() +
                        (self.n_features - sPI)*np.log(sB))
                       for sA, sPI, sB in zip(self.a, self.pi, self.b)]

        # Update the Q matrices
        self.Q = [sQ[:, :sPI] for sQ, sPI in zip(self.Q, self.pi)]

    def e_step(self, X):
        """Compute the e-step of the algorithm

        Parameters
        ----------
        X : tensor, shape (n_samples, n_dimensions)
            Tensor of n_features-dimensional data points. Each row
            corresponds to a single data point.
        Returns
        -------

        """
        # Compute the membership function
        K = self.score_samples(X)
        K *= 0.5

        # logsumexp trick
        LL = torch.logsumexp(K, 1).sum()

        # Compute the posterior
        SM = torch.nn.Softmax(dim=1)
        self.T = SM(K)

        return LL.item()

    def score_samples(self, X):
        """Compute the negative weighted log probabilities for each sample.

        Parameters
        ----------
        X : tensor, shape (n_samples, n_features)
            List of n_features-dimensional data points. Each row
            corresponds to a single data point.

        Returns
        -------
        log_prob :tensor, shape (n_samples, n_clusters)
            Log probabilities of each data point in X.
        """
        nt, d = X.shape
        K = torch.empty((nt, self.C), dtype=self.dtype)

        # Start the prediction for each class
        for c in range(self.C):
            # Compute the constant term
            K[:, c] = self.logdet[c] - 2*torch.log(self.prop[c]) + self.cst

            # Remove the mean
            Xc = X - self.mean[c]

            # Compute the discriminative function
            K[:, c] += torch.sum(Xc**2, 1)/self.b[c] \
                - torch.sum(torch.mm(Xc, self.Q[c] *
                                     torch.sqrt(1/self.b[c]-1/self.a[c]))**2,
                            1)

            # Px = torch.mm(Xc,
            #               torch.mm(self.Q[c], self.Q[c].t()))
            # temp = torch.mm(Px, self.Q[c]/torch.sqrt(self.a[c]))
            # K[:, c] += torch.sum(temp**2, 1, keepdim=True)
            # K[:, c] += torch.sum((Xc-Px)**2, 1, keepdim=True)/self.b[c]

        return -K

    def predict(self, X):
        """Predict the labels for the data samples in X using trained model.

        Parameters
        ----------
        X : tensor shape (n_samples, n_features)
            List of n_features-dimensional data points. Each row
            corresponds to a single data point.

        Returns
        -------
        labels : tensor, shape (n_samples,)
            Component labels.
        """
        return self.score_samples(X).argmax(1)

    def predict_proba(self, X):
        """
        Predict the membership probabilities for the data samples
        in X using trained model.

        Parameters
        ----------
        X : tensor, shape (n_samples, n_features)
            List of n_features-dimensional data points. Each row
            corresponds to a single data point.

        Returns
        -------
        proba : tensor, shape (n_samples, n_clusters)
        """
        # Compute the membership function
        K = self.score_samples(X)

        # Compute the posterior
        SM = torch.nn.Softmax(dim=1)
        T = SM(K)

        return T

    def free(self):
        """This  function free some  parameters of the  model.

        Use in the EM algorithm
        """
        self.pi = []
        self.a = []
        self.b = []
        self.logdet = []
        self.q = None

        self.ni = []          # Number of samples of each class
        self.prop = []        # Proportion of each class
        self.mean = []        # Mean vector
        self.pi = []            # Signal subspace size
        self.L = []           # Eigenvalues of covariance matrices
        self.Q = []
        self.trace = []

    def signal_subspace_estimation(self):
        if self.subspace_estimation == "cattel":
            if self.model in ('M1', 'M3', 'M5', 'M7'):
                for c in range(self.C):
                    # Scree test
                    min_dim = (min(self.ni[c], self.n_features) - 1)
                    dL, pi = (self.L[c][1:]-self.L[c][:-1]).abs(), 1
                    dL /= dL.max()
                    while (torch.any(dL[pi:] > self.th) and
                           (pi+1 <= min_dim)):
                        pi += 1
                    self.pi.append(pi)

            elif self.model in ('M2', 'M4', 'M6', 'M8'):
                p = 1
                min_dim = int(min(min(self.ni), self.n_features))
                while (torch.any(self.dL[p:] > self.th) and
                       (p+1 <= min_dim)):
                    p += 1
                self.pi = [p for c in range(self.C)]
                del p

    def estim_signal_subspace(self):
        """ Estimation of the signal subspace variance
        """
        self.a = [sL[:sPI] for sL, sPI in zip(self.L, self.pi)]
        if self.model in ('M5', 'M6', 'M7', 'M8'):
            self.a = [sA.mean().repeat(sA.shape)
                      for sA in self.a]

    def estim_noise_subspace(self):
        if self.model in ('M1', 'M2', 'M5', 'M6'):
            # Noise free
            self.b = [(sT-sA.sum())/(self.n_features-sPI)
                      for sT, sA, sPI in zip(self.trace, self.a, self.pi)]
            # Check for very small value of b
            self.b = [b if b > EPS else EPS for b in self.b]

        elif self.model in ('M3', 'M4', 'M7', 'M8'):
            # Noise common
            denom = self.n_features - np.sum([sPR*sPI for sPR, sPI in
                                              zip(self.prop, self.pi)])
            num = np.sum([sPR*(sT-sA.sum()) for sPR, sT, sA in
                          zip(self.prop, self.trace, self.a)])

            # Check for very small values
            if num < EPS:
                self.b = [EPS for i in range(self.C)]
            elif denom < EPS:
                self.b = [1/EPS for i in range(self.C)]
            else:
                am = np.min([sA.min() for sA in self.a])
                b = num/denom
                self.b = [b if am > b else am - EPS
                          for i in range(self.C)]

    def compute_nb_parameters(self):
        # Compute the number of parameters of the model
        self.q = self.C*self.n_features + (self.C-1) + \
            sum([sPI*(self.n_features-(sPI+1)/2) for sPI in self.pi])

        # Number of noise subspaces
        if self.model in ('M1', 'M3', 'M5', 'M7'):
            self.q += self.C
        elif self.model in ('M2', 'M4', 'M6', 'M8'):
            self.q += 1
        # Size of signal subspaces
        if self.model in ('M1', 'M2'):
            self.q += sum(self.pi) + self.C
        elif self.model in ('M3', 'M4'):
            self.q += sum(self.pi) + 1
        elif self.model in ('M5', 'M6'):
            self.q += 2*self.C
        elif self.model in ('M7', 'M8'):
            self.q += self.C+1

   
    def _get_param_names(self):
        return ["model", "th", "C", "dtype"]
        
    def get_params(self, deep=True):
        """
        Get parameters for this estimator.

        Parameters
        ----------
        deep : bool, default=True
            If True, will return the parameters for this estimator and
            contained subobjects that are estimators.

        Copied from sklearn codebase

        Returns
        -------
        params : dict
            Parameter names mapped to their values.
        """
        out = dict()
        for key in self._get_param_names():
            value = getattr(self, key)
            if deep and hasattr(value, 'get_params'):
                deep_items = value.get_params().items()
                out.update((key + '__' + k, val) for k, val in deep_items)
            out[key] = value
        return out

    def set_params(self, **params):
        """
        Set the parameters of this estimator.

        The method works on simple estimators as well as on nested objects
        (such as :class:`~sklearn.pipeline.Pipeline`). The latter have
        parameters of the form ``<component>__<parameter>`` so that it's
        possible to update each component of a nested object.

        Copied from sklearn codebase
        Parameters
        ----------
        **params : dict
            Estimator parameters.

        Returns
        -------
        self : estimator instance
            Estimator instance.
        """
        if not params:
            # Simple optimization to gain speed (inspect is slow)
            return self
        valid_params = self.get_params(deep=True)

        nested_params = defaultdict(dict)  # grouped by prefix
        for key, value in params.items():
            key, delim, sub_key = key.partition('__')
            if key not in valid_params:
                raise ValueError('Invalid parameter %s for estimator %s. '
                                 'Check the list of available parameters '
                                 'with `estimator.get_params().keys()`.' %
                                 (key, self))

            if delim:
                nested_params[key][sub_key] = value
            else:
                setattr(self, key, value)
                valid_params[key] = value

        for key, sub_params in nested_params.items():
            valid_params[key].set_params(**sub_params)

        return self
